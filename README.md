﻿
# Usage

* Add reference to WebGardenCom assembly.

* Add initialization block in application startup block :

```csharp
        JobManager.Instance.Init(r => {
            r.RegisterJobType<string, Resp>(7, p =>
            {
                System.Threading.Thread.Sleep(5000);
                return new Resp { 
                    date=DateTime.Now.ToString(), 
                    text = "Hello " + p, 
                    pid = System.Diagnostics.Process.GetCurrentProcess().Id, 
                    domain = AppDomain.CurrentDomain.Id 
                };
            });
        });
```

* Add execution code anywhere :

```csharp
        var ret=JobManager.Instance.GetLauncher<Resp>(7)("World !");
```

# Reference

## Initialization

Call JobManager.Init with a callback taking a IRegister parameter then call IRegister methods and properties to initialize job manager.

* IRegister.RegisterJobType<*InputType*,*OutputType*>(*JobTypeId*,*JobHandler*)

    * InputType : Class containing input parameters for job. Must be Serializable.

    * OutputType : Class containing output results for job. Must be Serializable.

    * JobTypeId : Job identifier as short int. Must be unique in application.

    * JobHandler : Job implementation as Func<*InputType*,*OutputType*>.


## Job execution

* Func<object,List<*OutputType*>> JobManager.GetLauncher<*OutputType*>(*JobTypeId*)

    * OutputType : Class containing output results for job. Must be Serializable.

    * JobTypeId : Job identifier as short int. Must be unique in application.

    Returns a delegate to call to execute a job. 

    This delegate takes an object as parameter and returns a List<*OutputType*> containing results from each web garden members.
    
    Input parameter should be castable as *InputType* used to register job.

* Task<List<*OutputType*>> Launch<*OutputType*>(*JobTypeId*, *Parameters*, *Callback*)

    * OutputType : Class containing output results for job. Must be Serializable.

    * JobTypeId : Job identifier as short int. Must be unique in application.

    * Parameters : Input parameters that should be castable as *InputType* used to register job.
    
    * Callback : Optional callback called when all job instances are done. Call back is an Action<List<*OutputType*>>.
    
    Returns a running Task which will complete when all job instances are done.