﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Microsoft.Ajax.Utilities;
using WebGardenCom;

namespace WebGardenTest
{
    public class MvcApplication : System.Web.HttpApplication
    {
        RandomNumberGenerator rnd = RandomNumberGenerator.Create();

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            JobManager.Instance.Init(r => {

                r.RegisterJobType<string, Resp>(7, p =>
                {
                    var b = new byte[2];
                    rnd.GetBytes(b);
                    int i = ((b[0]*256 + b[1])%750)*10 + 500;
                    System.Threading.Thread.Sleep(i);
                    return new Resp { date=DateTime.Now, text = "Hello " + p, i=i, pid = System.Diagnostics.Process.GetCurrentProcess().Id, domain = AppDomain.CurrentDomain.Id };

                });

            });

        }
    }

    [Serializable]
    public class Resp
    {
        public DateTime date;
        public string text;
        public int i;
        public int pid;
        public int domain;
    }
}
