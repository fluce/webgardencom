﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGardenCom;

namespace WebGardenTest.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "";
            var logger = JobManager.Instance.Logger as MemoryLogger;
            ViewBag.Log = logger.GetLog();
            return View();
        }

        public ActionResult Signal(short jobType)
        {
            var t = JobManager.Instance.GetLauncher<object>(jobType)("World !");
            var logger = JobManager.Instance.Logger as MemoryLogger;
            var l = logger.GetLog();
            return Json(new { result = t, logs = l });
        }

        public ActionResult GetLog()
        {
            var logger = JobManager.Instance.Logger as MemoryLogger;
            return Json(logger.GetLog());
        }

        public ActionResult ClearLog()
        {
            var logger=JobManager.Instance.Logger as MemoryLogger;
            logger.ClearLog();
            return Json(logger.GetLog());
        }

    }
}