﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Threading.Tasks;

namespace WebGardenCom
{
    public sealed class JobManager : IDisposable
    {
        private static JobManager instance=new JobManager();
        public static JobManager Instance {  get { return instance; } }

        Semaphore semaphore;
        Semaphore semaphoreRel;
        EventWaitHandle launcherEvent;

        private const string SemaphoreName = @"Global\webgardensemaphore";
        private const string SemaphoreNameRel = @"Global\webgardensemaphoreRel";
        private const string LauncherEventName = @"Global\webgardenlaunchermutex";

        public int JobWaitTimeout { get; internal set; }
        public int JobManagerBusyTimeout { get; internal set; }
        public int ListenerLoopTimeout { get; internal set; }

        public byte ProcessIndex { get; private set; }

        SharedData SharedData { get; set; }

        public ILogger Logger { get; internal set; }

        internal Dictionary<short, JobHandler> Handlers { get; set; }

        internal string ApplicationId { get; set; }

        CancellationTokenSource cancellationTokenSource=new CancellationTokenSource();
        
        private JobManager()
        {
            Logger = new MemoryLogger();
            Handlers = new Dictionary<short, JobHandler>();
            JobWaitTimeout = 30000;
            JobManagerBusyTimeout = 1000;
            ListenerLoopTimeout = 30000;
        }

        public void Init( Action<IRegister> register)
        {
            if (launcherEvent != null)
            {
                Logger.LogError("Job manager already initialized");
                throw new Exception("Already initialized");
            }

            ApplicationId = System.Web.Hosting.HostingEnvironment.ApplicationID;

            register(new InternalRegister(this));

            var tf = new TaskFactory();
            bool created;
            var worldSid = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
            var sec = new SemaphoreSecurity();
            sec.AddAccessRule(new SemaphoreAccessRule(worldSid, SemaphoreRights.FullControl, AccessControlType.Allow));
            var sece = new EventWaitHandleSecurity();
            sece.AddAccessRule(new EventWaitHandleAccessRule(worldSid, EventWaitHandleRights.FullControl, AccessControlType.Allow));

            launcherEvent = new EventWaitHandle(true, EventResetMode.AutoReset, LauncherEventName + ApplicationId, out created, sece);
            Logger.LogDebug("LauncherMutex created : {0}", created);

            semaphore = new Semaphore(0, SharedData.MaxProcess, SemaphoreName + ApplicationId, out created, sec);
            Logger.LogDebug("Semaphore created : {0}", created);

            semaphoreRel = new Semaphore(0, SharedData.MaxProcess, SemaphoreNameRel + ApplicationId, out created, sec);
            Logger.LogDebug("SemaphoreRel created : {0}", created);

            SharedData = new SharedData(ApplicationId, Logger);

            ProcessIndex = SharedData.GetNextFreeProcessIndex();

            Logger.LogDebug("PID {0} - Domain {2} - process #{1}", System.Diagnostics.Process.GetCurrentProcess().Id, ProcessIndex,AppDomain.CurrentDomain.Id);
            
            tf.StartNew(JobListener, cancellationTokenSource.Token);
        }

        private void JobListener()
        {
            int lastJobIndex = -1;
            while (!cancellationTokenSource.IsCancellationRequested)
            {
                SharedData.UpdateLastUpdateDate(ProcessIndex);
                Logger.LogDebug("# Process updates dates : {0}", SharedData.GetLastUpdateDates().Aggregate("", (s, x) => s + " " + x.ToLongTimeString()));

                if (WaitHandle.WaitAny(new[] { cancellationTokenSource.Token.WaitHandle, semaphore }, ListenerLoopTimeout)==1)
                {
                    int currentJobIndex = SharedData.CurrentJobIndex;
                    short jobType = SharedData.JobType;
                    if (currentJobIndex > lastJobIndex)
                    {
                        Logger.LogInfo("## signaled, job #{0} starting : {1}", currentJobIndex, jobType);

                        JobHandler handler;
                        if (Handlers.TryGetValue(jobType,out handler))
                        {
                            var resp = handler.CallAction(SharedData.JobParameters);
                            SharedData.SetJobProcessResponse(ProcessIndex, resp);
                            Logger.LogInfo("## job done");
                        }
                        else {
                            Logger.LogWarning("## job type not registered ({0})", jobType);
                        }
                        lastJobIndex = currentJobIndex;
                    }
                    else
                    {
                        Logger.LogWarning("## signaled, job #{0} ignored (mismatched LastJobIndex={1})", currentJobIndex,lastJobIndex);
                    }
                    semaphoreRel.Release();
                }
            }
            Logger.LogInfo("Job Listener stopped");
        }


        public Func<object,List<T>> GetLauncher<T>(short jobType) where T:class
        {
            return x => { var t=Launch<T>(jobType, x); t.Wait(); return t.Result; };
        }

        public Task<List<T>> Launch<T>(short jobType, object prms, Action<List<T>> callback=null) where T:class
        {
            if (!launcherEvent.WaitOne(JobManagerBusyTimeout))
            {
                Logger.LogWarning("> Launcher is busy");
                throw new Exception("Launcher is busy");
            }
            
            try
            {
                JobHandler handler;

                if (!Handlers.TryGetValue(jobType,out handler))
                {
                    Logger.LogWarning("job type not registered ({0})", jobType);
                    throw new Exception("job type not registered");
                }

                SharedData.SetJobParam(handler.PrepareParameters(prms));

                var jobIndex = SharedData.GetNextJobIndex(jobType);
                var processCount = SharedData.ProcessCount;
                Logger.LogInfo("> signaling {0} processes for job #{1}", processCount, jobIndex);
                int jcount = 0;
                int ret;
                do
                {
                    try
                    {
                        jcount++; ret = semaphore.Release();
                    }
                    catch (Exception)
                    {
                        ret = -1;
                    }
                } while (ret != -1 && jcount != processCount);

                var tf = new TaskFactory();

                return tf.StartNew(() =>
                {
                    try
                    {
                        Logger.LogDebug(">> waiting for {0} job done signals", jcount);
                        while (jcount-- > 0)
                        {
                            if (semaphoreRel.WaitOne(JobWaitTimeout))
                            {
                                Logger.LogDebug(">> job done signaled, {0} to go", jcount);
                            }
                            else
                            {
                                Logger.LogDebug(">> no more job signaled");
                                break;
                            }
                        }
                        Logger.LogInfo(">> job is completely done");

                        var resp = handler.DecodeResponse<T>(SharedData.GetJobProcessResponses());
                        if (callback != null)
                            callback(resp);
                        return resp;
                    }
                    finally
                    {
                        launcherEvent.Set();
                    }
                });
            }
            catch
            {
                launcherEvent.Set();
                throw;
            }
        }

        public void Dispose()
        {
            if (semaphore != null) semaphore.Dispose();
            if (semaphoreRel != null) semaphoreRel.Dispose();
            if (launcherEvent != null) launcherEvent.Dispose();
            if (cancellationTokenSource!=null) cancellationTokenSource.Dispose();
            if (SharedData != null)
            {
                var s=SharedData;
                SharedData=null;
                s.Dispose();
            }
        }
    }
}
