using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace WebGardenCom
{
    internal class JobHandler
    {
        public Func<object, object> Action { get; set; }
        public Type ParameterType { get; set; }
        public Type ResponseType { get; set; }

        public byte[] CallAction(byte[] parameters)
        {
            BinaryFormatter formatter = new BinaryFormatter();

            var inStream = new MemoryStream(parameters);
            var prm = formatter.Deserialize(inStream);

            object response;
            try
            {
                response = Action(prm);
            }
            catch (Exception e)
            {
                response = e;
            }

            var outStream = new MemoryStream(SharedData.JobResponseLength);
            formatter.Serialize(outStream, response);
            return outStream.GetBuffer();
        }

        public byte[] PrepareParameters(object o)
        {
            if (o.GetType().IsInstanceOfType(ParameterType))
                throw new Exception("Invalid type");

            BinaryFormatter formatter = new BinaryFormatter();

            var outStream = new MemoryStream(SharedData.JobParamLength);
            formatter.Serialize(outStream, o);
            return outStream.GetBuffer();

        }

        public List<T> DecodeResponse<T>(List<byte[]> response) where T : class
        {
            BinaryFormatter formatter = new BinaryFormatter();

            var ret = new List<T>(response.Count);

            foreach (var i in response)
            {
                var inStream = new MemoryStream(i);
                try
                {
                    var o = formatter.Deserialize(inStream) as T;
                    ret.Add(o);
                }
                catch (System.Runtime.Serialization.SerializationException)
                {
                    ret.Add(null);
                }

            }
            return ret;

        }

    }
}