using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace WebGardenCom
{
    public class MemoryLogger:ILogger
    {
        public enum Level
        {
            Error,
            Warning,
            Info,
            Debug
        }

        readonly List<string> log = new List<string>();
        readonly object lck = new object();

        readonly TraceSwitch traceSwitch=new TraceSwitch("WebGardenJobManager","Cross Web Garden job manager","2");

        public void LogMessage(Level level, string format, params object[] parms)
        {
            var s = DateTime.Now + " " + level.ToString().ToUpperInvariant() + " " + string.Format(format, parms);
            Debug.WriteLine(s);
            switch (level)
            {
                case Level.Error:
                    if (traceSwitch.TraceError)
                        Trace.TraceError(format, parms);
                    break;
                case Level.Warning:
                    if (traceSwitch.TraceWarning)
                        Trace.TraceWarning(format, parms);
                    break;
                case Level.Info:
                    if (traceSwitch.TraceInfo)
                        Trace.TraceInformation(format, parms);
                    break;
                case Level.Debug:
                    if (traceSwitch.TraceVerbose)
                        Trace.WriteLine(string.Format("DEBUG " + format, parms));
                    break;
            }
            lock (lck)
            {
                log.Add(s);
            }
        }

        public void ClearLog()
        {
            lock (lck)
            {
                log.Clear();
            }
        }

        public List<string> GetLog()
        {
            lock (lck)
            {
                return log.ToList();
            }
        }

        public void LogDebug(string format, params object[] parms)
        {
            LogMessage(Level.Debug, format, parms);
        }

        public void LogInfo(string format, params object[] parms)
        {
            LogMessage(Level.Info, format, parms);
        }

        public void LogError(string format, params object[] parms)
        {
            LogMessage(Level.Error, format, parms);
        }

        public void LogWarning(string format, params object[] parms)
        {
            LogMessage(Level.Warning, format, parms);
        }
    }

    public interface ILogger
    {
        void LogDebug(string format, params object[] parms);
        void LogInfo(string format, params object[] parms);
        void LogError(string format, params object[] parms);
        void LogWarning(string format, params object[] parms);
    }
}