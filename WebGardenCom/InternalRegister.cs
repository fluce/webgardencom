using System;

namespace WebGardenCom
{
    public interface IRegister
    {
        void RegisterJobType<T, TU>(short jobType, Func<T, TU> handler) where T : class where TU : class;

        string ApplicationId { get; set; }

        ILogger Logger { get; set; }

        int JobWaitTimeout { get; set; }
        int JobManagerBusyTimeout { get; set; }
        int ListenerLoopTimeout { get; set; }

    }

    internal class InternalRegister : IRegister
    {
        private readonly JobManager webGardenJobManager;

        public InternalRegister(JobManager webGardenJobManager)
        {
            this.webGardenJobManager = webGardenJobManager;
        }

        public void RegisterJobType<T, TU>(short jobType, Func<T, TU> handler) where T : class where TU : class
        {
            webGardenJobManager.Handlers[jobType] = new JobHandler
            {
                Action = x => handler(x as T),
                ParameterType = typeof(T),
                ResponseType = typeof(TU)
            };
        }
        public string ApplicationId { get { return webGardenJobManager.ApplicationId; } set { webGardenJobManager.ApplicationId = value; } }

        public ILogger Logger { get { return webGardenJobManager.Logger; } set { webGardenJobManager.Logger = value; } }

        public int JobWaitTimeout
        {
            get { return webGardenJobManager.JobWaitTimeout; }
            set { webGardenJobManager.JobWaitTimeout = value; }
        }

        public int JobManagerBusyTimeout
        {
            get { return webGardenJobManager.JobManagerBusyTimeout; }
            set { webGardenJobManager.JobManagerBusyTimeout = value; }
        }

        public int ListenerLoopTimeout
        {
            get { return webGardenJobManager.ListenerLoopTimeout; }
            set { webGardenJobManager.ListenerLoopTimeout = value; }
        }

    }
}