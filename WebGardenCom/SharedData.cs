using System;
using System.Collections.Generic;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Threading;

namespace WebGardenCom
{
    internal class SharedData : IDisposable
    {
        internal const int MaxProcess = 16;

        internal const int JobParamLength = 512;
        internal const int JobResponseLength = 65536;

        private const int OFFSET_PROCESS_COUNT = 0;
        private const int OFFSET_JOB_TYPE = 2;
        private const int OFFSET_CURRENT_JOB = 4;
        private const int OFFSET_PROCESS_DATA_DATE_ARRAY = 8;
        private const int OFFSET_JOB_PARAM = OFFSET_PROCESS_DATA_DATE_ARRAY + MaxProcess * sizeof(long);
        private const int OFFSET_PROCESS_DATA_JOB_RESPONSE_ARRAY = OFFSET_JOB_PARAM + JobParamLength;

        internal const int TotalLength = OFFSET_PROCESS_DATA_JOB_RESPONSE_ARRAY + MaxProcess * JobResponseLength;

        private const string MutexName = @"Global\webgardenmutex";
        private const string SharedMemName = @"Global\webgardensharedMem";

        ILogger Logger { get; set; }

        readonly Mutex sharedMemMutex;
        readonly MemoryMappedFile sharedMemFile;
        readonly MemoryMappedViewAccessor sharedMemAccessor;

        public SharedData(string applicationName, ILogger logger)
        {
            Logger = logger;

            bool created;
            var worldSid = new SecurityIdentifier(WellKnownSidType.WorldSid, null);

            var secm = new MutexSecurity();
            secm.AddAccessRule(new MutexAccessRule(worldSid, MutexRights.FullControl, AccessControlType.Allow));
            sharedMemMutex = new Mutex(false, MutexName+ applicationName, out created, secm);
            Logger.LogDebug("SharedMemMutex created : {0}", created);

            var secmf = new MemoryMappedFileSecurity();
            secmf.AddAccessRule(new AccessRule<MemoryMappedFileRights>(worldSid, MemoryMappedFileRights.FullControl, AccessControlType.Allow));
            sharedMemFile = MemoryMappedFile.CreateOrOpen(SharedMemName+applicationName, TotalLength, MemoryMappedFileAccess.ReadWrite, MemoryMappedFileOptions.None, secmf, System.IO.HandleInheritability.Inheritable);
            Logger.LogDebug("sharedMemFile created");

            sharedMemAccessor = sharedMemFile.CreateViewAccessor();

        }

        public byte ProcessCount
        {
            get
            {
                return sharedMemAccessor.ReadByte(OFFSET_PROCESS_COUNT);
            }
        }

        public int CurrentJobIndex
        {
            get
            {
                return sharedMemAccessor.ReadInt32(OFFSET_CURRENT_JOB);
            }
        }

        public short JobType
        {
            get
            {
                return sharedMemAccessor.ReadInt16(OFFSET_JOB_TYPE);
            }
        }

        public byte[] JobParameters
        {
            get
            {
                var ret = new byte[JobParamLength];
                sharedMemAccessor.ReadArray(OFFSET_JOB_PARAM,ret,0, JobParamLength);
                return ret;
            }
        }

        public int GetNextJobIndex(short jobType)
        {
            sharedMemMutex.WaitOne();
            var currentJobIndex = sharedMemAccessor.ReadInt32(OFFSET_CURRENT_JOB);
            currentJobIndex++;
            sharedMemAccessor.Write(OFFSET_CURRENT_JOB, currentJobIndex);
            sharedMemAccessor.Write(OFFSET_JOB_TYPE, jobType);
            sharedMemMutex.ReleaseMutex();
            return currentJobIndex;
        }

        public byte GetNextFreeProcessIndex()
        {
            sharedMemMutex.WaitOne();
            var count = sharedMemAccessor.ReadByte(OFFSET_PROCESS_COUNT);
            var ticksArray = new long[count];
            sharedMemAccessor.ReadArray(OFFSET_PROCESS_DATA_DATE_ARRAY, ticksArray, 0, count);
            var dateArray = ticksArray.Select(x => DateTime.FromBinary(x)).ToArray();
            Logger.LogDebug("GetNextFreeProcessIndex : Updates dates : {0}", dateArray.Aggregate("", (s, x) => s + " " + x.ToLongTimeString()));
            byte b = 0;
            while (b < count)
            {
                if (dateArray[b] < DateTime.Now.AddMinutes(-10))
                {
                    Logger.LogDebug("GetNextFreeProcessIndex : index {0} stalled", b + 1);
                    break;
                }
                b++;
            }
            b++;

            if (b > MaxProcess)
            {
                Logger.LogError("MAX_PROCESS exceeded ({0})", b);
                throw new Exception(string.Format("MAX_PROCESS exceeded ({0})", b));
            }

            if (b > count)
                sharedMemAccessor.Write(OFFSET_PROCESS_COUNT, b);
            var ticks = DateTime.Now.ToBinary();
            sharedMemAccessor.Write(OFFSET_PROCESS_DATA_DATE_ARRAY + (b - 1) * sizeof(long), ticks);
            sharedMemMutex.ReleaseMutex();
            return b;
        }

        public List<DateTime> GetLastUpdateDates()
        {
            sharedMemMutex.WaitOne();
            var count = sharedMemAccessor.ReadByte(OFFSET_PROCESS_COUNT);
            var ticksArray = new long[count];
            sharedMemAccessor.ReadArray(OFFSET_PROCESS_DATA_DATE_ARRAY, ticksArray, 0, count);
            sharedMemMutex.ReleaseMutex();
            return ticksArray.Select(x => DateTime.FromBinary(x)).ToList();
        }

        public void UpdateLastUpdateDate(int idx)
        {
            sharedMemMutex.WaitOne();
            var ticks = DateTime.Now.ToBinary();
            sharedMemAccessor.Write(OFFSET_PROCESS_DATA_DATE_ARRAY + (idx - 1) * sizeof(long), ticks);
            sharedMemMutex.ReleaseMutex();
        }

        public void SetJobParam(byte[] prms)
        {
            sharedMemMutex.WaitOne();
            sharedMemAccessor.WriteArray(OFFSET_JOB_PARAM, prms, 0, JobParamLength);
            sharedMemMutex.ReleaseMutex();
        }
        public void SetJobProcessResponse(int idx, byte[] prms)
        {
            sharedMemMutex.WaitOne();
            sharedMemAccessor.WriteArray(OFFSET_PROCESS_DATA_JOB_RESPONSE_ARRAY+(idx-1)*JobResponseLength, prms, 0, JobParamLength);
            sharedMemMutex.ReleaseMutex();
        }

        public List<byte[]> GetJobProcessResponses()
        {
            sharedMemMutex.WaitOne();
            var count = sharedMemAccessor.ReadByte(OFFSET_PROCESS_COUNT);
            var ret = new List<byte[]>(count);
            for (int i = 0; i < count; i++)
            {
                var b = new byte[JobResponseLength];
                sharedMemAccessor.ReadArray(OFFSET_PROCESS_DATA_JOB_RESPONSE_ARRAY + i * JobResponseLength, b, 0, JobResponseLength);
                ret.Add(b);
            }
            sharedMemMutex.ReleaseMutex();
            return ret;
        }

        public void Dispose()
        {
            if (sharedMemMutex != null)
                sharedMemMutex.Dispose();
            if (sharedMemAccessor != null)
                sharedMemAccessor.Dispose();
            if (sharedMemFile!=null)
                sharedMemFile.Dispose();
        }

    }
    }